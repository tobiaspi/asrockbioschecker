import argparse
import requests
import re
from datetime import datetime
from pprint import pprint

from bs4 import BeautifulSoup
from telegram.ext import Updater


"""DEBUG output switch"""
DEBUG = False


class Version:
    """
    Class representiong a BIOS version consisting of a version string and a date.
    """
    def __init__(self, version: str, date: str = ""):
        self.__version = version
        self.__date = date

    def __lt__(self, other):
        """
        :param other: The other version to compare to
        :return: A boolean value.
        """
        this_version = re.sub("[^0-9.]", "", self.__version)
        other_version = re.sub("[^0-9.]", "", other.__version)
        if DEBUG:
            print("Comparing: {} - {}".format(this_version, other_version))
        return this_version < other_version

    def __repr__(self):
        """
        Return a string representation of the object
        :return: The string "{} - {}".format(self.__version, self.__date)
        """
        return "{} - {}".format(self.__version, self.__date)


def get_version(url: str) -> Version:
    """
    Get the version from the ASRock website

    :param url: The URL to the changelog page
    :type url: str
    :return: A Version object.
    """
    body = requests.get(url).text
    soup = BeautifulSoup(markup=body, features="html.parser")
    if DEBUG:
        pprint(soup)
        print("-" * 80)

    entries = soup.find_all("tr")
    versions = []
    for entry in entries:
        # Version is the first element, the date the second. Hope ASRock does not change this.
        entries = entry.find_all("td")
        if not entries or not entries[0].text:
            continue

        # There must be at least two entries, otherwise this is not valid
        if len(entries) >= 2:
            # Let's format the date like a sane person
            date_unformatted = entries[1].string
            date = datetime.strptime(date_unformatted, "%Y/%m/%d").date().isoformat()
            versions.append(Version(entries[0].text, date))

    if DEBUG:
        pprint(versions)

    # Better show the latest version first, users like it
    versions.sort(reverse=True)
    # Nicely output the versions
    pprint(versions)
    # return the highest version
    return versions[0]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Checker for ASRock BIOS updates')

    parser.add_argument('-u', '--url', action="store", dest="url",
                        default="https://www.asrock.com/mb/AMD/B450%20Pro4/BIOS.de.html")
    parser.add_argument('-v', '--version', action="store", dest="version")
    parser.add_argument('-t', "--token", action="store", dest="token", required=True)
    parser.add_argument('-i', "--chat_id", action="store", dest="chat_id", required=True)

    arguments = parser.parse_args()

    updater = Updater(token=arguments.token, use_context=True)

    version = get_version(arguments.url)

    message = "Checking for new BIOS version...\nChecking URL: {}{}\nLatest found version is: {}".format(
        arguments.url,
        "\nYour current BIOS version is: {}".format(arguments.version) if arguments.version else "",
        version)

    if arguments.version:
        if DEBUG:
            print("Version given: {}".format(arguments.version))
        current_version = Version(arguments.version)
        if current_version < version:
            updater.bot.send_message(arguments.chat_id, text=message)
    else:
        updater.bot.send_message(arguments.chat_id, text=message)
